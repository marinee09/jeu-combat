<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita5d6acffede74aa3e15320ad81aad500
{
    public static $prefixLengthsPsr4 = array (
        'm' => 
        array (
            'marine\\MiniJeuDeCombat\\' => 23,
        ),
        'L' => 
        array (
            'Libs\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'marine\\MiniJeuDeCombat\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
        'Libs\\' => 
        array (
            0 => __DIR__ . '/../..' . '/libs',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita5d6acffede74aa3e15320ad81aad500::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita5d6acffede74aa3e15320ad81aad500::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
