<?php namespace marine\MiniJeuDeCombat\Managers;
use PDO;
use marine\MiniJeuDeCombat\Models\Personnage;
use Libs\SuperPDO;




class PersonnagesManager
{
    private $superPdo;

    public function __construct($superPdo)
    {
        $this->setBdd($superPdo);
    }

    //On ajoute un nouveau personnage
    public function add(Personnage $perso)
    {

        $req = $this->superPDO->prepare('INSERT INTO personnages_v2(nom, type) VALUES(:nom, :type)');
        $req->bindValue(':nom', $perso->nom());
        $req->bindValue('type', $perso->type());
        $req->execute();

        $newPerso = $this->superPdo->query(
            'INSERT INTO personnages (nom) VALUES (?)',
            [$perso->nom]
        );

        $perso->hydrate([
            'id' => $this->superPdo->pdo->lastInsertId(),
            'degats' => 0,
            'experience' => 0,
            'niveau' => 1,
            'nbCoups' => 0,
            'dateDernierCoup' => '0000-00-00',
            'atout' =>0
        ]);
    }

    public function count()
    {
        return $this->superPdo->query('SELECT COUNT(*) FROM personnages')->fetchColumn();
        return $this->superPdo->query('SELECT COUNT(*) FROM personnages_v2')->fetchColumn();
    }

    public function delete(Personnage $perso)
    {
        $this->superPdo->query('DELETE FROM personnages WHERE id = ' . $perso->id);
        $this->db->exec('DELETE FROM personnages_v2 WHERE id = '.$perso->id());
    }

    public function exists($info)
    {
        if (is_int($info)) {
            $req = $this->superPdo->query(
                'SELECT count(*) FROM personnages WHERE id = ?',
                [$info]
            );
            return $req->fetchColumn();
            return (bool) $this->superPdo->query('SELECT COUNT(*) FROM personnages_v2 WHERE id = '.$info)->fetchColumn();
        }

        // Sinon c'est qu'on veut verifier que le nom existe ou pas
        $req = $this->superPdo->prepare('SELECT COUNT(*) FROM personnages_v2 WHERE nom = :nom');
        $req->execute([':nom' =>$info]);
        return (bool) $req->fetchColumn();

        $req = $this->superPdo->query('SELECT COUNT(*) FROM personnages WHERE nom = ?',[$info]);
        return (bool)$req->fetchColumn();
    }

    public function get($info)
    {
        if (is_int($info)) {
            $req = $this->superPdo->query(
                'SELECT id, nom, degats, experience, niveau, nbCoups, dateDernierCoup FROM personnages WHERE id = ?',
                [$info]
            );
            $donnees = $req->fetch(PDO::FETCH_ASSOC);
            return new Personnage($donnees);

            $req = $this->superPdo->query(
                'SELECT id, nom, degats, experience, niveau, nbCoups, dateDernierCoup FROM personnages WHERE nom = ?',
                [$info]
            );
            $donnees = $req->fetch(PDO::FETCH_ASSOC);
            return new Personnage($donnees);

            if (is_int($info)) 
            {
            $req = $this->superPdo->query(
                'SELECT id, nom, degats, timeEndormi, type, atout FROM personnages_v2 WHERE id = '.$info);
            $perso = $req->fetch(PDO::FETCH_ASSOC);
            }
            else{
                $req = $this->superPdo->prepare('SELECT id, nom, degats, timeEndormi, type, atout FROM personnages_v2 WHERE nom = :nom');
                $req->execute(['nom' => $info]);

                $perso = $req->fetch(PDO::FETCH_ASSOC);
            }

            switch ($perso['type'])
            {
                case 'guerrier' : return new Guerrier($perso);
                case 'magicien' : return new Magicien($perso);
                default: return null;
            }
        }
    }

    public function getList($nom)
    {
        $persos = [];
        $req = $this->superPdo->query(
            'SELECT * FROM personnages WHERE nom <> ? ORDER BY nom',
            [$nom]
        );

        while ($donnees = $req->fetch(PDO::FETCH_ASSOC)) {
            $persos[] = new Personnage($donnees);
        }
        return $persos;


        $persos = [];
        $req = $this->superPdo->prepare('SELECT * FROM personnages_v2 WHERE nom <> :nom ORDER BY nom');
        $req->execute(['nom' => $nom]);

        while ($donnees = $req->fetch(PDO::FETCH_ASSOC))
        {
            switch ($donnees['type'])
            {
                case 'guerrier': $perso[] = new Guerrier($donnees); break;
                case 'magicien': $perso[] = new Magicien($donnees); break;
            }
        }

        return $persos;
    }

    public function update(Personnage $perso)
    {
        $req = $this->superPdo->query(
            'UPDATE personnages SET degats = ?, experience = ?, niveau = ?, nbCoups = ?, dateDernierCoup = ? WHERE id = ?',
            [
                $perso->degats,
                $perso->experience,
                $perso->niveau,
                $perso->nbCoups,
                $perso->dateDernierCoup->format('Y-m-d'),
                $perso->id
            ]
        );




        $req = $this->superPdo->prepare('UPDATE personnages_v2 SET degats = :degats, timeEndormi = :timeEndormi, atout = :atout WHERE id = :id');

        $req->bindValue(':degats', $perso->degats(), PDO::PARAM_INT);
        $req->bindValue(':timeEndormi', $perso->timeEndormi(), PDO::PARAM_INT);
        $req->bindValue(':atout', $perso->atout(), PDO::PARAM_INT);
        $req->bindValue(':id', $perso->id(), PDO::PARAM_INT);

        $req->execute();
    }

    public function setBdd(SuperPDO $superPdo)
    {
        $this->superPdo = $superPdo;
    }
}