

<fieldset>
<center>
    <legend><b>Mes informations</b></legend>
    </center>   
    <p>
    <center>
        <b></i>Nom</i> : <?= htmlspecialchars($perso->nom) ?></b><br/>
        <b><i>Dégâts</i> : <?= $perso->degats ?></b><br>
        <b><i>Expérience</i> : <?= $perso->experience ?></b><br>
        <b><i>Niveau</i> : <?= $perso->niveau ?></b><br>
        <b><i>Nombre des coups</i> : <?= $perso->nbCoups ?></b><br>
        <b><i>Date de dernier coup</i> : <?= $perso->dateDernierCoup->format('d/m/Y') ?></b>
    </center>
    </p>
</fieldset>
<fieldset>
    <center>
    <legend><b>Qui frapper ?</b></legend><br>
    </center>
    <p>
        <?php

        foreach ($ennemis as $unPerso) {
            echo '<a href="?frapper=' . $unPerso->id . '">' .
                htmlspecialchars($unPerso->nom) .
                '</a>
                    (dégâts : ' . $unPerso->degats . ', expérience : ' .
                $unPerso->experience . ', niveau : ' . $unPerso->niveau .
                ', nombre des coups : ' . $unPerso->nbCoups . ', date de dernier coup : ' .
                $unPerso->dateDernierCoup->format('d/m/Y') . ')<br />';
        }

        ?>
    </p>
</fieldset>

<br><br>
<p><a href="?deconnexion=1">Déconnexion</a></p>